/* 
 * The main application to test out dynamic memory manager
 */

#include <stdio.h>
#include <mm.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>

#define DMM_MALLOC_FREE_VALIDATION 1
#define DMM_MALLOC_VS_MALLOC_TEST 0

#define DATA_SET_SIZE 6
void *pointer[DATA_SET_SIZE] = { NULL };
unsigned int size[DATA_SET_SIZE] = {10, 250, 1000, 2500, 6000, 500};

void parse_with_getchar(void)
{
	getchar();

	dmm_parse();;

	getchar();
}

void dmm_alloc_wrapper(unsigned int index)
{
	pointer[index] = dmm_malloc(size[index]);
	
	printf("dmm_malloc(%d) done! - Address returned = %x\n", size[index], pointer[index]);

	parse_with_getchar();	
	
	return;
}

void dmm_free_wrapper(unsigned int index)
{

	dmm_free( pointer[index] );
	
	printf("dmm_free(%x) [ %d Bytes] done!\n", pointer[index], size[index]);

	parse_with_getchar();	

	return;
}


int main(void)
{

	struct timeval tv;
	int iter = 0;

#if DMM_MALLOC_VS_MALLOC_TEST	
	suseconds_t start_malloc_time;
	suseconds_t end_malloc_time;

	suseconds_t start_dmm_time;
	suseconds_t end_dmm_time;
#endif

#if DMM_MALLOC_FREE_VALIDATION
	printf("Starting dmm_malloc/dmm_free validation...\n");

	parse_with_getchar();

	dmm_alloc_wrapper(0);
	dmm_alloc_wrapper(1);
	dmm_alloc_wrapper(2);
	dmm_alloc_wrapper(3);
	dmm_alloc_wrapper(4);

	// Free

	dmm_free_wrapper(2);
	dmm_alloc_wrapper(5);
	dmm_free_wrapper(5);
	dmm_free_wrapper(3);
	dmm_free_wrapper(1);
	dmm_free_wrapper(4);
	dmm_free_wrapper(0);
	
	printf("End of dmm_malloc/dmm_free validation\n");

#endif

#if DMM_MALLOC_VS_MALLOC_TEST
	gettimeofday( &tv, NULL );
	start_dmm_time = tv.tv_usec;
	
	for( iter = 0; iter < 1000; ++iter )
	{
		pointer[iter] = dmm_malloc(1000);
	}

	for( iter = 0; iter < 1000; ++iter )
	{
		dmm_free(pointer[iter]);
	}

	gettimeofday( &tv, NULL );
	end_dmm_time = tv.tv_usec;


	gettimeofday( &tv, NULL );
	start_malloc_time = tv.tv_usec;

	for( iter = 0; iter < 1000; ++iter )
	{
		pointer[iter] = malloc(1000);
	}


	for( iter = 0; iter < 1000; ++iter )
	{
		free(pointer[iter]);

	}

	gettimeofday( &tv, NULL );
	end_malloc_time = tv.tv_usec;

	printf("Time of dmm execution --> %lu usecs\n", (unsigned long)( end_dmm_time - start_dmm_time ));
	printf("Time of malloc execution --> %lu usecs\n", (unsigned long)( end_malloc_time - start_malloc_time ));

#endif

	return 0;
}
