#ifndef __MM_H
#define __MM_H

#define MAX_NODES_PER_PAGE ( 208 )
#define NODES_PER_PAGE ( 194 ) // This is a calculated value
#define FIRST_NODE_INDEX (0)
#define PAGE_SIZE ( 4096 )
#define WORD_SIZE	(4)

#define BLOCK_FREE ( 0 )
#define BLOCK_USED ( 1 )

#define SUCCESS   (  0 )
#define ERROR     ( -1 )
#define ERROR_MEM ( -2 )

#define CLEAN_PAGE	( 0 )
#define DIRTY_PAGE	( 1 )

#define BLOCK_POS_FIRST ( 0 )
#define BLOCK_POS_MIDDLE ( 1 )
#define BLOCK_POS_LAST ( 2 )

#define DEBUG_BUILD		(0)

#define LOG(...) printf(__VA_ARGS__)

#if DEBUG_BUILD
#define LOG_DBG(...) printf(__VA_ARGS__)
#else
#define LOG_DBG(...)
#endif

typedef struct metahead {

	unsigned char      index_map[MAX_NODES_PER_PAGE]; // Holds a map of indices 
	unsigned int       metanode_count;				 // Total number of active metanodes in the current metapage
	struct metahead* next;						 // Pointer to the next metahead instance

} metahead_t;

typedef struct metanode {
	
	void *       data_page_addr;   // Pointer to allocated page 
	void * 		 last_alloc_block; // Address of last allocated block
	unsigned int page_count;	   // Total number of pages allocated by this metanode
	unsigned int lcfb;			   // Largest continuous free byte
	unsigned int scfb;			   // Smallest continuous free byte

} metanode_t;

typedef struct page_metadata {

	unsigned int flag : 2;			// Flag bits at the end of each meta entry in the data page
	unsigned int size : 30;		// Size which points to the next data allocation space in the data page

} page_metadata_t;

/*
 *	dmm_malloc - Provides the caller with the starting virtual address of a chunk of memory from the heap, specified by @size
 *	@size - Size of memory chunk required in bytes
 *
 *	returns - Address of memory chunk from the heap
 */

void * dmm_malloc( unsigned int size );

/*
 *	dmm_malloc - Frees a chunk of memory previously allocated using dmm_malloc
 *	@addr - Address of the memory chunk
 * 
 *  @prerequisite - The address should be the start address of the memory chunk obtained by calling dmm_malloc
 *
 *	returns - nothing
 */

void dmm_free( void * addr );


#endif
