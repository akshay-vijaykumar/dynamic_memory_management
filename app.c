/* 
 * The main application to test out dynamic memory manager
 */

#include <stdio.h>
#include <mm.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>

int main(void)
{

	void *pointer[1000] = { NULL };
	struct timeval tv;
	suseconds_t start_malloc_time;
	suseconds_t end_malloc_time;

	suseconds_t start_dmm_time;
	suseconds_t end_dmm_time;

	int iter = 0;
#if 0
	unsigned int val;
	unsigned int head;

	printf("Sizeof metahead = %d\n", sizeof(metahead_t));
	printf("Sizeof metanode = %d\n", sizeof(metanode_t));

	head = sizeof(metahead_t);
	val = PAGE_SIZE - head;

	printf("Total Bytes remaining in metapage = %d\n", val);

	val = val / sizeof(metanode_t);

	printf("Total metanodes that can exist in a metapage = %d\n", val);
#endif
	gettimeofday( &tv, NULL );
	start_dmm_time = tv.tv_usec;

#if 1
	getchar();
	pointer[0] = dmm_malloc( 10 );
	getchar();
	pointer[1] = dmm_malloc(250);
	pointer[2] = dmm_malloc(6000);
	pointer[3] = dmm_malloc(1);
	pointer[4] = dmm_malloc(4096);
	pointer[5] = dmm_malloc(10000);

	dmm_free( pointer[1] );
	dmm_free( pointer[4] );
	dmm_free( pointer[2] );
	dmm_free( pointer[5] );
	dmm_free( pointer[0] );
	dmm_free( pointer[3] );
#endif

	for( iter = 0; iter < 1000; ++iter )
	{
		pointer[iter] = dmm_malloc(1000);
	}

	for( iter = 0; iter < 1000; ++iter )
	{
		dmm_free(pointer[iter]);
	}

	gettimeofday( &tv, NULL );
	end_dmm_time = tv.tv_usec;


	gettimeofday( &tv, NULL );
	start_malloc_time = tv.tv_usec;

#if 0
	pointer[0] = malloc( 10 );
	pointer[1] = malloc(250);
	pointer[2] = malloc(6000);
	pointer[3] = malloc(1);
	pointer[4] = malloc(4096);
	pointer[5] = malloc(10000);

	free( pointer[1] );
	free( pointer[4] );:w
	free( pointer[2] );
	free( pointer[5] );
	free( pointer[0] );
	free( pointer[3] );
#endif

	for( iter = 0; iter < 1000; ++iter )
	{
		pointer[iter] = malloc(1000);
	}


	for( iter = 0; iter < 1000; ++iter )
	{
		free(pointer[iter]);

	}

	gettimeofday( &tv, NULL );
	end_malloc_time = tv.tv_usec;

	printf("Time of dmm execution --> %lu usecs\n", (unsigned long)( end_dmm_time - start_dmm_time ));
	printf("Time of malloc execution --> %lu usecs\n", (unsigned long)( end_malloc_time - start_malloc_time ));

	return 0;
}
