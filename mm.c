#include <mm.h>
#include <stdio.h>
#include <string.h>
#include <sys/mman.h>

/* Global Variables */
static metahead_t *MHEAD = NULL; // This is the head metapage node

#if DEBUG_BUILD
	static char * BLOCK_POS_ARRAY[3] = {"FIRST", "MIDDLE", "LAST"};
#endif

/* Static Functions */

/*
 *	metanode_index_to_ptr - This function returns the address of a particular metanode based with at @index into the metapage.
 *	@head_ptr - Pointer to metahead instance of a particular metapage.
 *  @index - Index of metanode. 
 *
 *	returns - Pointer to the metanode instance. NULL on Error.
 */

static metanode_t * metanode_index_to_ptr( metahead_t* head_ptr, int index )
{
	metanode_t * mnode_ptr = NULL;
	unsigned long mhead_addr = (unsigned long) head_ptr;
	unsigned long mnode_addr = 0;

//	if(( index_count <= head_ptr->metanode_count ) && ( index < NODES_PER_PAGE ))
	if( index < NODES_PER_PAGE )
	{
		mnode_addr = mhead_addr + sizeof( metahead_t );
		mnode_ptr = &((metanode_t *) mnode_addr)[index];
	}	

	return mnode_ptr;
}

/*
 *	create_data_page - 	creates a new data page using mmap and inserts a page_metadata entry at the top
 *	@no_of_pages - number of pages to allocate contiguously, this will be passed to mmap
 *	@page_ptr - allocated page address will be populated here
 *
 *	returns - 0 if successful, -1 if failed
 */

static int create_data_page( unsigned int no_of_pages, void **page_ptr )
{
	int retval = SUCCESS;
	void *page_address = NULL;
	unsigned int total_size = 0;
	unsigned long last_address = 0;

	LOG_DBG("Creating new data page for %d pages\n", no_of_pages);

	// Calculate total size based on number of requested pages
	total_size = no_of_pages * PAGE_SIZE;

	LOG_DBG("Total size is %d\n", total_size);

	// Request for no_of_pages continuous pages
	page_address = mmap( NULL, total_size, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0 );
	if(page_address == MAP_FAILED)
	{
		LOG_DBG("ERROR!! Cannot mmap data page\n");
		retval = ERROR;
	}
	else
	{
		LOG_DBG("Allocated data page at address --> 0x%x\n", (unsigned int)page_address);
		// Initializes top entry of new page with a page_metadata information
		page_metadata_t pmetadata;
		pmetadata.size = total_size;
		pmetadata.flag = BLOCK_FREE;

		last_address = (unsigned long)page_address;
		last_address += total_size - sizeof( page_metadata_t);	// We want to insert a footer tag, last byte of page at page + total_size - 1

		LOG_DBG("Placing header tag at address --> 0x%x\n", (unsigned int)page_address );
		LOG_DBG("Placing footer tag at address --> 0x%x\n", (unsigned int)last_address );

		memcpy( page_address, &pmetadata, sizeof(page_metadata_t) );
		memcpy( (void *)last_address, &pmetadata, sizeof(page_metadata_t) );

		// Assign page address to parameter
		*page_ptr = page_address;

	}

	LOG_DBG("Returning from create page function\n");
	return retval;
}


/*
 *	delete_data_page - deletes an already allocated page.
 *	pre-requisite - page must already be allocated with a call to create_data_page
 *
 *	@page_ptr - address of the variable holding page address that needs to be released
 *				This variable will be set to NULL after page is deallocated
 *  @no_of_pages - Number of pages to deallocate from the provided start address
 *
 *  returns - 0 if successful, -1 if failed
 */

static int delete_data_page( void **page_ptr, unsigned int no_of_pages )
{
	int retval = SUCCESS;
	unsigned int total_size = 0;

	// Calculate total memory size for unmapping
	total_size = no_of_pages * PAGE_SIZE;

	// Unmap memory starting from page_ptr;
	retval = munmap( *page_ptr, total_size );

	if(retval == SUCCESS)
	{
		*page_ptr = NULL;
	}

	return retval;
}

/*
 *	insert_metanode - insert the metanode index into a sorted order in the indexmap of metahead
 *
 *	NOTE: The indexmap contains an array of metanode indices. These indices are always in the sorted
 *		  order of allocated virtual addresses.
 *
 *	pre-requisite - a free metanode has been requested by calling get_free_metanode
 *
 *	@mhead - pointer to metahead in the metapage
 *	@idx - index of the metanode as returned by get_free_metanode
 *
 *	returns - 0 if successful, -1 if failed
 */

static int insert_metanode( metahead_t *mhead, int idx )
{	
	int binary_idx = 0;
	int first_idx = 0;
	int last_idx = 0;

	unsigned long binary_addr = 0;
	unsigned long insertion_addr = 0;

	metanode_t *binary_mnode = NULL;
	metanode_t *insertion_mnode = NULL;

	/*
	 *	Assumptions - Insertion algorithm needs to change if these are not met
	 *	1. This function is called immediately after get_free_metanode
	 *	2. This means, the current metanode_count is the index obtained for insertion
	 *	3. The data page for the node is allocated
	 *	4. The node index boundary has already been validated
	 */

	LOG_DBG("Inserting metanode for metahead address --> 0x%x, index %d\n", (unsigned int)mhead, idx);


	if( mhead == NULL )
	{
		LOG("Error: Cannot insert metanode. Parameter passed is NULL\n");
		return ERROR;
	}

	if( idx != mhead->metanode_count )
	{
		LOG("Error: Insertion algoritm failed!! Assumptions not valid\n");
		return ERROR;
	}

	insertion_mnode = metanode_index_to_ptr( mhead, idx );

	LOG_DBG("Insertion node address --> 0x%x\n", (unsigned int)insertion_mnode);

	if( insertion_mnode->data_page_addr == NULL )
	{
		LOG("Error: Node insertion failed. Data page not allocated for the node\n");
		return ERROR;
	}

	insertion_addr = (unsigned long)insertion_mnode->data_page_addr;
	binary_idx = ( mhead->metanode_count )/2;
	last_idx = idx;

	LOG_DBG("Insertion page address --> 0x%x\n", (unsigned int)insertion_addr);
	LOG_DBG("Metanode count is %d\n", mhead->metanode_count);

	while( binary_idx > first_idx && binary_idx < last_idx )
	{
		binary_mnode = metanode_index_to_ptr( mhead, binary_idx );
		binary_addr = (unsigned long)binary_mnode->data_page_addr;

		LOG_DBG("--Comparing addresses 0x%x and 0x%x\n", (unsigned int)insertion_addr, (unsigned int)binary_addr );
		LOG_DBG("--First index = %d, search index = %d, last index = %d\n", first_idx, binary_idx, last_idx);

		if( binary_addr == insertion_addr )
		{
			LOG("Error: Insertion address already exists\n");
			return ERROR;
		}

		if( insertion_addr > binary_addr )
		{
			LOG_DBG("Insertion address is greater\n");
			first_idx = binary_idx;
			binary_idx = ( binary_idx + last_idx )/2;
			if( first_idx == binary_idx )
			{
				++binary_idx;
				binary_mnode = metanode_index_to_ptr( mhead, binary_idx );
			}
			LOG_DBG("Moving search index to %d\n", binary_idx);
		}

		else
		{
			LOG_DBG("Insertion address is smaller\n");
			last_idx = binary_idx;
			binary_idx = ( first_idx + binary_idx )/2;
			if( last_idx == binary_idx )
			{
				--binary_idx;
				binary_mnode = metanode_index_to_ptr( mhead, binary_idx );
			}
			LOG_DBG("Moving search index to %d\n", binary_idx);
		}
	}

	LOG_DBG("Insertion index determined at %d\n", binary_idx);
	// We have an index where the new element should go
	if( binary_idx != mhead->metanode_count )
	{
		memmove( ( mhead->index_map ) + binary_idx + 1, mhead->index_map + binary_idx, ( mhead->metanode_count - binary_idx ) );
		mhead->index_map[binary_idx] = idx;
	}

	++mhead->metanode_count;

	LOG_DBG("Updated metanode count to %d\n", mhead->metanode_count);

#if 0
	LOG_DBG("----Sorted index map\n");
	for( binary_idx = 0; binary_idx < mhead->metanode_count; ++binary_idx )
	{
		LOG_DBG("----Index %d, mapped to %d\n", binary_idx, mhead->index_map[binary_idx]);
	}
#endif


	LOG_DBG("Returning from insert metanode\n");

	return SUCCESS;
}

/*
 *	remove_metanode - removes a metanode from the sorted list of indexmap
 *
 *	pre-requisite - The metanode is already inserted in the indexmap with a call to insert_metanode
 *
 *	@mhead - pointer to metahead in the metapage
 *	@idx - index of metanode as returned by get_free_metanode
 *
 *	returns - 0 if successful, -1 if failed
 */

static int remove_metanode( metahead_t *mhead, int idx )
{
	int iter = 0; 	// Iteration variable
	
	if( mhead == NULL )
	{
		LOG("Error: Cannot remove metanode. Parameter is NULL\n");
		return ERROR;
	}

	for( iter = 0; iter < mhead->metanode_count; ++iter )
	{
		if( mhead->index_map[iter] == idx )
		{
			memmove( mhead->index_map + iter, mhead->index_map + iter + 1, mhead->metanode_count - iter - 1 );
			mhead->index_map[mhead->metanode_count - 1] = idx;
			--(mhead->metanode_count);

			return SUCCESS;
		}
	}

	// We couldn't find the requested index in the allocated index map
	return ERROR;
}

/*
 *	get_free_metanode - finds a free metanode in the metapage having @mhead metahead,
 *						allocates a data-page for that node and inserts it into the index map
 *
 *	prerequisites - metapage exists with an initialized metahead
 *
 *	@mhead - header information embedded in the metapage
 *	@mnode - address of the free metanode
 *	@size  - size of allocation required for this node
 *
 *	returns - 	(-1) if no free node is found or an error is encountered
 *				index of the free metanode from the metapage. [There are an array of metanodes in a metapage].
 */

static int get_free_metanode( metahead_t *mhead, metanode_t **mnode_ptr, unsigned int size )
{
	unsigned char *map = NULL; // Local pointer to index map.
	int index = 0;
	metanode_t *mnode = NULL;
	int retval = SUCCESS;
	unsigned int no_of_pages = 1;
	unsigned int max_data_size = 0;

#if DEBUG_BUILD
	int iter = 0;
#endif
	

	LOG_DBG("Started search for free metanode for size %d\n", size);
	LOG_DBG("Started search for metahead address --> 0x%x\n", mhead);

	// Null point dereference check
	if( mhead == NULL || mnode_ptr == NULL )
	{
		LOG("Error : metahead or mnode pointer instance is NULL\n");
		return ERROR;
	}

	// Check if a free node exists
	if( mhead->metanode_count >= NODES_PER_PAGE )
	{
		LOG_DBG("Error : No free node in metapage\n");
		return ERROR;
	}

	// Get hold of the index_map from mhead
	map = mhead->index_map;

#if 0
	LOG_DBG("----Printing index map\n");
	for( iter = 0; iter < 10; ++iter )
	{
		LOG_DBG("----index %d, mapped to %d\n", iter, map[iter]);
	}
#endif

	index = map[mhead->metanode_count];
	LOG_DBG("Metanode count is %d\n", mhead->metanode_count);

	// Calculate address of metanode
	*mnode_ptr = metanode_index_to_ptr( mhead, index );
	mnode = *mnode_ptr;

	// Allocate data page for this node
	max_data_size = PAGE_SIZE - ( 2 * sizeof(page_metadata_t) );
	no_of_pages = size * ( 1/(double)max_data_size);
	if( size % max_data_size )
	{
		++no_of_pages;
	}

#if 0
	if( mhead->metanode_count == 0 )
	{
		no_of_pages = 4;
	}

#endif

	LOG_DBG("New node at address --> 0x%x, index %d\n", (unsigned int)mnode, index);
	LOG_DBG("Requesting new data page for new node at index %d, no of pages is %d\n", index, no_of_pages);

 	retval = create_data_page( no_of_pages, &(mnode->data_page_addr));
	if( retval != SUCCESS )
	{
		LOG_DBG("ERROR!! Cannot create data page. Memory allocation error!!\n");
		return ERROR_MEM;
	}

	mnode->page_count = no_of_pages;
	mnode->last_alloc_block = mnode->data_page_addr;
	mnode->lcfb = no_of_pages * PAGE_SIZE - ( 2 * sizeof(page_metadata_t) );
	mnode->scfb = no_of_pages * PAGE_SIZE - ( 2 * sizeof(page_metadata_t) );

	LOG_DBG("Created data page. Inserting node into index map\n");
	LOG_DBG("Data page address is --> %x\n", (unsigned long)mnode->data_page_addr);
	insert_metanode( mhead, index );

	LOG_DBG("Returning from get_free_metanode\n");
	return index;
}

/*
 * create_metapage - creates and initializes a metapage with a metahead
 * @page_ptr - address of the variable holding address of metapage
 *
 * returns - 0 if successful, -1 if failed
 */ 

static int create_metapage( void **page_ptr )
{
	unsigned int total_size = PAGE_SIZE;	// Meta page will always be one page size
	void *page_address = NULL;				// Allocated page address
	metahead_t *mhead = NULL;				// header for the metapage
	int iter = 0;							// Iterator
	int retval = 0;						

	LOG_DBG("Creating metapage\n");
	if( page_ptr == NULL )
	{
		LOG("Error: NULL pointer parameter\n");
		return ERROR;
	}

	page_address = mmap( NULL, total_size, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0 );
	
	if( page_address == MAP_FAILED )
	{
		retval = ERROR;
	}

	else
	{
		mhead = page_address;		// Insert header at the top of the metapage

		LOG_DBG("Initializing metapage\n");

		for( iter = 0; iter < NODES_PER_PAGE; ++iter )
		{
			mhead->index_map[iter] = iter;
		}

		mhead->metanode_count = 0;		// Not relying on mmap to give zeroed page, probably stupid but still...
		mhead->next = NULL;

		*page_ptr = page_address;

		retval = SUCCESS;
	}

	LOG_DBG("Created metapage --> Addr: 0x%x\n", (unsigned int)page_address);

	return retval;

}

/*
 *	delete_metapage - deletes an already allocated metapage.
 *	pre-requisite - metapage must already be allocated with a call to create_metapage
 *
 *	@page_ptr - address of the variable holding page address that needs to be released
 *				This variable will be set to NULL after page is deallocated
 *  
 *  returns - 0 if successful, -1 if failed
 */

static int delete_metapage( void **page_ptr )
{
	// Check if there is anything allocated through this metapage. If there is any free all of it and generate a warning message
	// NOTE: This call should probably not happen unless all the nodes in the metapage have been freed.
	metahead_t *mhead = NULL;
	int retval = 0;
	
	if( page_ptr == NULL )
	{
		LOG("Error: NULL pointer passed as parameter\n");
		return ERROR;
	}

	mhead = *page_ptr;
	
	if( mhead == NULL )
	{
		LOG("Error: Page pointer was NULL\n");
		return ERROR;
	}

	if( mhead->metanode_count > 0 )
	{
		LOG_DBG("Error: This page still has valid metanodes and should not be removed.\n");
		return ERROR;
	}

	// All data pages have been freed
	retval = munmap( mhead, PAGE_SIZE );

	if( retval == SUCCESS )
	{
		*page_ptr = NULL;
		LOG_DBG(" Setting Page Pointer to Null\n");
	}
	else
	{
		LOG("Error: munmap failed while freeing metapage\n");
	}

	return retval;
}

/*
 *	get_wordblock_count - returns the minimum number of words an allocation would need
 *
 *	@size - size of the allocation
 *
 *	return value - min wordcount
 */

static int get_wordblock_count( unsigned int size )
{
	int wc = 0;

	wc = size * (1/(double)WORD_SIZE);

	if( size % WORD_SIZE )
	{
		++wc;
	}

	return wc;
}

static int split_data_block( metanode_t *mnode, unsigned long current_address, unsigned int alloc_size )
{

	unsigned long lcfb = 0;
	unsigned long effective_free_space = 0;
	page_metadata_t *pmdata = (page_metadata_t *)current_address;
	page_metadata_t *end_pmdata = NULL;
	unsigned char dirty_flag = CLEAN_PAGE;


	LOG_DBG("Splitting data block\n");

	if( mnode == NULL || pmdata == NULL )
	{
		LOG("Error: Cannot split data block. Parameter is NULL\n");
		return ERROR;
	}

	LOG_DBG("Existing header tag at address --> 0x%x\n", (unsigned long)pmdata);

	if( pmdata->flag != BLOCK_FREE )
	{
		LOG("Error: Cannot split an allocated block\n");
		return ERROR;
	}

	lcfb = mnode->lcfb;
	effective_free_space = pmdata->size - (2 * sizeof(page_metadata_t));
	end_pmdata = (page_metadata_t *)( current_address + pmdata->size - sizeof(page_metadata_t) );

	if( effective_free_space < alloc_size )
	{
		LOG("Error: Cannot split data block. Not enough space\n");
		return ERROR;
	}

	LOG_DBG("Current free space in the block is %d bytes\n", effective_free_space);

	if( lcfb == effective_free_space )	// We are doing equals check because we want to make sure that the biggest chunk is being split
	{
		// Force a reparse of the entire page to look for the next lcfb, decrement here anyway
		LOG_DBG("Breaking known lcfb. Marking page as dirty\n");
		mnode->lcfb = lcfb - alloc_size;
		dirty_flag = DIRTY_PAGE;
	}

	LOG_DBG("Existing footer tag at address --> 0x%x\n", (unsigned long)end_pmdata);
	LOG_DBG("Existing size of block, footer tag is %d\n", end_pmdata->size);
	end_pmdata->size = end_pmdata->size - alloc_size;
	LOG_DBG("Modified size of block, footer tag is %d\n", end_pmdata->size);

	pmdata->size = alloc_size;
	LOG_DBG("Existing header tag at address --> 0x%x\n", (unsigned long)pmdata);
	LOG_DBG("Modified header tag size to %d\n", pmdata->size);

	pmdata = (page_metadata_t *)(current_address + alloc_size - (sizeof(page_metadata_t)));
	LOG_DBG("Inserting new footer tag at address --> 0x%x\n", (unsigned long)pmdata);
	pmdata->size = alloc_size;
	pmdata->flag = BLOCK_FREE;
	LOG_DBG("New footer tag size is %d\n", pmdata->size);

	++pmdata;
	LOG_DBG("Inserting new header tag at address --> 0x%x\n", (unsigned long)pmdata);
	pmdata->size = end_pmdata->size;
	LOG_DBG("New header tag size is %d\n", pmdata->size);
	pmdata->flag = BLOCK_FREE;

#if DEBUG_BUILD
	// Verifying things are fine
	if(( ((unsigned long)pmdata) + pmdata->size - sizeof(page_metadata_t) ) != end_pmdata )
	{
		LOG_DBG("Oops!! Doesn't seem like the new tags are okay!\n");
		LOG_DBG("Recheck your calculations!\n");
		getchar();
	}
#endif

	return dirty_flag;
}

/*
 *	alloc_data_block - This function allocates memory chunk of @size bytes in the data page managed by @mnode
 *	@mnode - Pointer to metanode instance.
 *  @size - Size of memory chunk in bytes.
 *
 *	returns - Address of allocated memory chunk on Success. NULL on Error.
 */

static void * alloc_data_block( metanode_t * mnode, unsigned int size )
{

	/*
	 * The starting tag of each data block is always word aligned.
	 * This ensures that the first free byte in the data block is also word aligned.
	 * We need to make sure that the end tag is also always word aligned.
	 */

	void *alloc_address = NULL;
	unsigned long alloc_size = 0;
	unsigned long current_address = 0;
	unsigned long end_dpage_address = 0;
	unsigned long end_meta_address = 0;
	unsigned long next_offset = 0;
	unsigned int flag = 0;
	unsigned long effective_size = 0;
	unsigned int wbc = 0;
	unsigned long lcfb = 0;
	unsigned long old_lcfb = 0;
	unsigned char dirty_flag = 0;
	unsigned long alloc_end_address = 0;

	LOG_DBG("Allocating Data block for size %d, for metanode address --> %x\n", size, (unsigned long) mnode);
	
	if( mnode == NULL )
	{
		LOG("Error: Cannot allocate datablock. Parameter is NULL\n");
		return NULL;
	}
	
	current_address = (unsigned long) (mnode->last_alloc_block);
	end_meta_address = current_address;
	end_dpage_address = (unsigned long) mnode->data_page_addr + (mnode->page_count * PAGE_SIZE) - 1;

	LOG_DBG("Starting Search Address --> %x\n", current_address);
	LOG_DBG("Start of end Metadata --> %x\n", end_meta_address);

	wbc = get_wordblock_count( size );		// These are the number of data words we need to allocate, add two tag fields to this
	alloc_size = ( wbc * WORD_SIZE ) + ( 2 * sizeof(page_metadata_t) );

	LOG_DBG("Minimum word allocation required is %d\n", wbc);
	LOG_DBG("Effective allocation size is %d\n", alloc_size);

	if( alloc_size > mnode->lcfb )
	{
		LOG_DBG("Error: Cannot allocate effective size in this data page\n");
		LOG_DBG("Largest chunk of data available is %d\n", mnode->lcfb);
	}

//	while( current_address != end_meta_address || ( ((page_metadata_t *) current_address)->size ) == ( mnode->page_count * PAGE_SIZE ) ) 
	do
	{
		next_offset = ((page_metadata_t *) current_address)->size;
		flag = ((page_metadata_t *) current_address)->flag;
		effective_size = next_offset - ( 2 * sizeof(page_metadata_t) );

		if( effective_size > lcfb )
		{
			// Track the lcfb we have seen while traversing through this page
			old_lcfb = lcfb;
			lcfb = effective_size;
		}

		if( size == effective_size && dirty_flag != DIRTY_PAGE && flag != BLOCK_USED )
		{
			// Try best fit
			LOG_DBG("Found a perfect match\n");
			alloc_address = (void *)(current_address + sizeof(page_metadata_t));
			mnode->last_alloc_block = (void *)current_address;
			((page_metadata_t *) current_address)->flag = BLOCK_USED;
			alloc_end_address = current_address + (((page_metadata_t *) current_address)->size) - sizeof(page_metadata_t);
			((page_metadata_t *) alloc_end_address)->flag = BLOCK_USED;

			// Manage lcfb
			if( size == mnode->lcfb )
			{
				LOG_DBG("Breaking known lcfb. Reparse page\n");
				dirty_flag = DIRTY_PAGE;
				mnode->lcfb = 0;
//				lcfb = old_lcfb;
			}

			return alloc_address;
		}

		else if( ( alloc_size <= effective_size ) && ( flag == BLOCK_FREE ) && (dirty_flag != DIRTY_PAGE) && flag != BLOCK_USED )
		{
			// Try splitting the block
			LOG_DBG("Found a data block with possible match\n");
			LOG_DBG("Allocation size is %d\n", alloc_size);
			LOG_DBG("Effective free space is %d\n", effective_size);
			LOG_DBG("Calling split block\n");

			// Split metadata
			dirty_flag = split_data_block( mnode, current_address, alloc_size );

			if( dirty_flag != ERROR )
			{
				alloc_address = (void *)(current_address + sizeof(page_metadata_t));
				mnode->last_alloc_block = (void *)current_address;
				((page_metadata_t *) current_address)->flag = BLOCK_USED;
				alloc_end_address = current_address + (((page_metadata_t *) current_address)->size) - sizeof(page_metadata_t);
				((page_metadata_t *) alloc_end_address)->flag = BLOCK_USED;
			}
			
			if( dirty_flag != DIRTY_PAGE )
			{
				return alloc_address;
			}

			else if( dirty_flag == ERROR )
			{
				LOG("Error: Something went wrong while splitting data block\n");
				return NULL;
			}

			//Page is dirty! Walk through all of it
			lcfb = old_lcfb;
		}

		else if( size <= effective_size && dirty_flag != DIRTY_PAGE && flag != BLOCK_USED )
		{	
			// Nothing works. Give the block as it is
			LOG_DBG("Returning first fit that we found\n");
			alloc_address = (void *)current_address + sizeof(page_metadata_t);
			mnode->last_alloc_block = (void *)current_address;
			((page_metadata_t *) current_address)->flag = BLOCK_USED;
			alloc_end_address = current_address + (((page_metadata_t *) current_address)->size) - sizeof(page_metadata_t);
			((page_metadata_t *) alloc_end_address)->flag = BLOCK_USED;

			// Manage lcfb
			if( size == mnode->lcfb )
			{
				LOG_DBG("Breaking known lcfb. Reparse page\n");
				dirty_flag = DIRTY_PAGE;
				mnode->lcfb = 0;
//				lcfb = old_lcfb;
			}

			return alloc_address;
		}

		// Get to the next metadata
		current_address = current_address + next_offset;
		LOG_DBG("Current Address --> %x\n", current_address);

		if(current_address > end_dpage_address)
		{
			current_address = (unsigned long) mnode->data_page_addr;

			LOG_DBG("Reseting search to start of page --> %x\n", current_address);
		}

	} while( current_address != end_meta_address );

	// Probably got out dirty
	if( lcfb > mnode->lcfb )
	{
		LOG_DBG("Setting lcfb to %d\n", mnode->lcfb);
//		mnode->lcfb = lcfb;
	}

	LOG_DBG("Final lcfb set to %d\n", mnode->lcfb);
	LOG_DBG("Returning from allocate_data_block\n");
	
	return alloc_address;
}

static unsigned int merge_current_next(page_metadata_t * current_start_meta, page_metadata_t * next_end_meta)
{
	unsigned int new_block_size = 0;
	unsigned int new_data_size = 0;

	LOG_DBG("Merging current and next block\n");
	LOG_DBG("Current blocks size before merging is %d\n", current_start_meta->size);
	LOG_DBG("Next block's size before merging is %d\n", next_end_meta->size);

	LOG_DBG("Current block meta start address --> 0x%x\n", (unsigned long) current_start_meta);
	LOG_DBG("Next block meta end address --> 0x%x\n", (unsigned long) next_end_meta);


	new_block_size = current_start_meta->size + next_end_meta->size;
	current_start_meta->size = new_block_size;
	next_end_meta->size = new_block_size;

	LOG_DBG("Current blocks size after merging is %d\n", current_start_meta->size);
	LOG_DBG("Next block's size after merging is %d\n", next_end_meta->size);

	new_data_size = new_block_size - ( 2 * sizeof(page_metadata_t) );

	LOG_DBG("New Data size for the free block %d\n", new_data_size);

	return new_data_size;
}

static unsigned int merge_current_prev(page_metadata_t * prev_start_meta, page_metadata_t * current_end_meta)
{
	unsigned int new_block_size = 0;
	unsigned int new_data_size = 0;

	LOG_DBG("Merging current and previous block\n");
	LOG_DBG("Current blocks size before merging is %d\n", current_end_meta->size);
	LOG_DBG("Previous block's size before merging is %d\n", prev_start_meta->size);
	
	LOG_DBG("Current block meta end address --> 0x%x\n", (unsigned long) current_end_meta);
	LOG_DBG("Prev block meta start address --> 0x%x\n", (unsigned long) prev_start_meta);

	new_block_size = current_end_meta->size + prev_start_meta->size;
	current_end_meta->size = new_block_size;
	prev_start_meta->size = new_block_size;

	LOG_DBG("Current blocks size after merging is %d\n", current_end_meta->size);
	LOG_DBG("Next block's size after merging is %d\n", prev_start_meta->size);

	new_data_size = new_block_size - ( 2 * sizeof(page_metadata_t) );

	LOG_DBG("New Data size for the free block %d\n", new_data_size);


	return new_data_size;
}

static void update_lc_sc_fb( metanode_t * mnode, unsigned int new_block_size )
{
	LOG_DBG("Updating LC and SC Free Byte counts int mnode with addr --> %x\n", (unsigned long) mnode);

	if( new_block_size > mnode->lcfb )
	{
		LOG_DBG("Current blocks size > metanode's LCFB\n");
		mnode->lcfb = new_block_size;
	}

	if( new_block_size < mnode->scfb )
	{
		LOG_DBG("Current block's size < metanode's SCFB\n");
		mnode->scfb = new_block_size;
	}

	LOG_DBG("Updating LC and SC Done. LCFB Value = %d\n", mnode->lcfb);

	return;
}

static void combine_data_block( metanode_t *mnode, unsigned long current_address, unsigned long current_start_meta, unsigned long current_end_meta)
{
	unsigned long start_address = 0;
	unsigned long end_address = 0;
	unsigned long prev_start_meta = 0;
	unsigned long prev_end_meta = 0;
	unsigned long next_start_meta = 0;
	unsigned long next_end_meta = 0;
	unsigned int prev_size = 0;
	unsigned int next_size = 0;
	unsigned int block_position = BLOCK_POS_MIDDLE;
	unsigned int new_block_size = 0;
	unsigned int prev_flag = 0;
	unsigned int next_flag = 0;

	LOG_DBG("Starting to combine data blocks\n");

	start_address = (unsigned long) mnode->data_page_addr;
	end_address = start_address + (mnode->page_count * PAGE_SIZE);

	prev_end_meta = current_start_meta - sizeof(page_metadata_t);
	//prev_size = ((page_metadata_t *) prev_end_meta)->size;
//	prev_flag = ((page_metadata_t *) prev_end_meta)->flag;
//	prev_start_meta = prev_end_meta + prev_size - sizeof(page_metadata_t);

	next_start_meta = current_end_meta + sizeof(page_metadata_t);
//	next_size = ((page_metadata_t *) next_start_meta)->size;
//	next_flag = ((page_metadata_t *) next_start_meta)->flag;
//	next_end_meta = next_start_meta + next_size - sizeof(page_metadata_t);

	// Determine the position of the block in the entire page.
	if(current_start_meta <= start_address)
	{
		next_size = ((page_metadata_t *) next_start_meta)->size;
		next_flag = ((page_metadata_t *) next_start_meta)->flag;
		next_end_meta = next_start_meta + next_size - sizeof(page_metadata_t);
		
		// The block that was freed is the first block in the page
		block_position = BLOCK_POS_FIRST;		
	}
	else if(next_start_meta >= end_address)
	{
		prev_size = ((page_metadata_t *) prev_end_meta)->size;
		prev_flag = ((page_metadata_t *) prev_end_meta)->flag;
		prev_start_meta = prev_end_meta - prev_size + sizeof(page_metadata_t);
		
		// The block that was freed is the last block in the page.
		block_position = BLOCK_POS_LAST;
	}
	else 
	{
		prev_size = ((page_metadata_t *) prev_end_meta)->size;
		prev_flag = ((page_metadata_t *) prev_end_meta)->flag;
		prev_start_meta = prev_end_meta - prev_size + sizeof(page_metadata_t);
		
		next_size = ((page_metadata_t *) next_start_meta)->size;
		next_flag = ((page_metadata_t *) next_start_meta)->flag;
		next_end_meta = next_start_meta + next_size - sizeof(page_metadata_t);
		
		// The block that was freed is somewhere in the middle 
		block_position = BLOCK_POS_MIDDLE;
	}

	LOG_DBG("Data Block Position is at %s\n", BLOCK_POS_ARRAY[block_position]);

	// Combine blocks
	if( (block_position == BLOCK_POS_FIRST) || (block_position == BLOCK_POS_MIDDLE) )
	{
		// Merge if next block is free
		if( next_flag == BLOCK_FREE )
		{
			new_block_size = merge_current_next((page_metadata_t *) current_start_meta, (page_metadata_t *) next_end_meta);

			// Modify current_end_meta for combining MIDDLE blocks.
			current_end_meta = next_end_meta;
			
			// Update lcfs and scfs if necessary
			update_lc_sc_fb( mnode, new_block_size );
		}
		else
		{
			LOG_DBG("Next block is not free\n");
		}
	}

	if( (block_position == BLOCK_POS_LAST) || (block_position == BLOCK_POS_MIDDLE) )
	{
		if( prev_flag == BLOCK_FREE )
		{
			new_block_size = merge_current_prev((page_metadata_t *) prev_start_meta, (page_metadata_t *) current_end_meta);
			
			// Update lcfs and scfs if necessary
			update_lc_sc_fb( mnode, new_block_size );
		}
		else
		{
			LOG_DBG("Prev Block is not free\n");
		}
	}


	return;
}

static void free_data ( metanode_t * mnode, page_metadata_t *start_meta_ptr, page_metadata_t *end_meta_ptr)
{
	unsigned int size = 0;

	LOG_DBG("Modifying Start meta at --> %x\n", (unsigned long) start_meta_ptr);
	LOG_DBG("Modifying End meta at --> %x\n", (unsigned long) end_meta_ptr);

	// Update flags
	start_meta_ptr->flag = BLOCK_FREE;
	end_meta_ptr->flag = BLOCK_FREE;

	LOG_DBG("Updated block flags to free\n");

	// Update LCFB and SCFB 
	size = (start_meta_ptr->size) - (2 * sizeof(page_metadata_t)); // Calculating actual data size, without headers
	update_lc_sc_fb( mnode, size );

	return;

}

static int validate_addr( metanode_t * mnode, unsigned long current_address, page_metadata_t **start_meta_ptr, page_metadata_t **end_meta_ptr )
{
	unsigned int size = 0;
	unsigned long start_meta_address = 0;
	unsigned long end_meta_address = 0;
	page_metadata_t *start_ptr;
	page_metadata_t *end_ptr;

	// Perform address validation by checking 
	// 1. Last 2 bits of the current block's page_metadata is 01
	// 2. The tail tag should be replica of head tag

	start_meta_address = current_address - sizeof(page_metadata_t);
	start_ptr = (page_metadata_t *) start_meta_address;

	size = start_ptr->size;

	end_meta_address = start_meta_address + size - sizeof(page_metadata_t);
	end_ptr = (page_metadata_t *) end_meta_address;

	LOG_DBG("Starting address validation\n");

	// Checking if metadatas are equal
	if( (start_ptr->size != end_ptr->size ) || (start_ptr->flag != end_ptr->flag) )
	{
		LOG("Error: Data's header and footer metas don't match\n");
		return ERROR;
	}
	
	LOG_DBG("Data's header and footers matched\n");

	if( start_ptr->flag != BLOCK_USED )
	{
		LOG("Error: Data block is already free.\n");
		return ERROR;
	}

	*start_meta_ptr = start_ptr;
	*end_meta_ptr = end_ptr;

	LOG_DBG("address of start meta --> %x\n", (unsigned long) *start_meta_ptr);
	LOG_DBG("address of end meta --> %x\n", (unsigned long) *end_meta_ptr);

	LOG_DBG("Data block is indeed allocated\n");
	LOG_DBG("Address validation successful\n");

	return SUCCESS;
}

/*
 *	free_data_block - This function frees a particular data block in the data page.
 *	@mnode - Pointer to metahead instance.
 *  @block_addr - Start address of the memory chunk to be freed.
 *
 *	returns - 0 on Success. -1 on Error.
 */

static int free_data_block( metanode_t * mnode, void * block_addr )
{
	int retval = ERROR;
	unsigned long current_address = 0;
	page_metadata_t *start_meta_ptr = NULL; 
	page_metadata_t *end_meta_ptr = NULL;

	current_address = (unsigned long) block_addr;

	retval = validate_addr ( mnode, current_address, &start_meta_ptr, &end_meta_ptr );
	if(retval == ERROR)
	{
		LOG("Error: Free address validation failed\n");
		return retval;
	}

	// Mark metaheaders and footers as free. 
	free_data( mnode, start_meta_ptr, end_meta_ptr );

	// Coalasce block with neihbouring free blocks
	combine_data_block( mnode, current_address, (unsigned long) start_meta_ptr, (unsigned long) end_meta_ptr);
	
	return SUCCESS;
}

/*
 *	check_for_init - This function checks if the atleast one metapage exists.
 *	@head_ptr - Pointer to metahead instance .
 *
 *	returns - 0 on Success. -1 on Error.
 */

static int check_for_init( void *head_ptr ) 
{
	// Checks head pointer to first metapage.
	if( head_ptr != NULL)
	{
		return SUCCESS;
	}

	return ERROR;
}

/*
 *	dmm_init - 	This function would allocate the first instance of the metapage.
 *
 *	returns - 0 if successful, -1 if failed
 */

static int dmm_init( void **head_ptr )
{
	// Create first metapage
	LOG_DBG("Creating first metapage\n");
	return create_metapage( head_ptr );
}



/*
 *	search_metanode - This function returns the index of a particular metanode with the current metapage.
 *	@mhead - Pointer to metahead instance of a particular metapage.
 *  @size - Of the memory chunck required in butes. 
 *
 *	returns - Index value from 0 to (NODES_PER_PAGE - 1) on finding a metanode. -1 on Error.
 */

static int search_metanode( metahead_t *mhead, unsigned int size )
{
	metanode_t *mnode = NULL;
	int node_idx = ERROR;
	int node_count = 0;

	LOG_DBG("Started search for metanode for size %d\n", size);
	LOG_DBG("Looking in metahead address --> 0x%x\n", (unsigned int)mhead);
	if( mhead == NULL )
	{
		LOG("Error: Cannot search metanode. NULL parameter passed\n");
		return node_idx;
	}

	mnode = metanode_index_to_ptr( mhead, FIRST_NODE_INDEX );

	if( mnode == NULL )
	{
		LOG("Error: Something went wrong while getting address of first node in the metapage\n");
		return node_idx;
	}

	for( node_count = 0; node_count < mhead->metanode_count; ++node_count )
	{
		//LOG_DBG("Looking in node index %d\n", node_count );
		if( size <= mnode[node_count].lcfb )
		{
			// We have our node here
			node_idx = node_count;

			LOG_DBG("Found node for allocation\n");
			LOG_DBG("Node address --> 0x%x\n", (unsigned int)&mnode[node_count]);
			LOG_DBG("Node index --> %d\n", node_count);

			return node_idx;
		}
	}

	// We went through all the allocated nodes. Nothing had free space. Create a new one
	LOG_DBG("No match for metanode for size %d\n", size);
	LOG_DBG("Requesting new metanode\n");
	node_idx = get_free_metanode( mhead, &mnode, size );

	LOG_DBG("Returning from search metanode\n");
	return node_idx;
}


/*
 *	search_metanode_addr - This function returns the index of a particular metanode with the current metapage.
 *	@mhead - Pointer to metahead instance of a particular metapage.
 *  @size - Of the memory chunck required in butes. 
 *
 *	returns - Index value from 0 to (NODES_PER_PAGE - 1) on finding a metanode. -1 on Error.
 */

static int search_metanode_addr( metahead_t *mhead, unsigned long addr )
{
	metanode_t *mnode = NULL;
	int node_idx = ERROR;
	int node_count = 0;
	unsigned long start_addr = 0;
	unsigned long end_addr = 0;
	unsigned int index = 0;

	LOG_DBG("Started search for metanode for addr %x\n", addr);
	LOG_DBG("Looking in metahead address --> 0x%x\n", (unsigned int)mhead);
	if( mhead == NULL )
	{
		LOG("Error: Cannot search metanode. NULL parameter passed\n");
		return node_idx;
	}

	mnode = metanode_index_to_ptr( mhead, FIRST_NODE_INDEX );

	if( mnode == NULL )
	{
		LOG("Error: Something went wrong while getting address of first node in the metapage\n");
		return node_idx;
	}

	for( node_count = 0; node_count < mhead->metanode_count; ++node_count )
	{
		
		index = mhead->index_map[node_count];
		
		LOG_DBG("Looking in node index %d\n", index);
		
		start_addr = (unsigned long) mnode[index].data_page_addr;
		end_addr = start_addr + ( mnode[index].page_count * PAGE_SIZE );

		if(( addr >= start_addr ) && ( addr <= end_addr ))
		{ 
			LOG_DBG("Found metanode for addr %x, at index %d\n", (unsigned long) mnode, index);
			node_idx = index;
			break;
		}
	}

	LOG_DBG("Returning from search metanode\n");
	return node_idx;
}

/*
 *	search_metapages - This function searches for a suitable metanode from all the metapages depending on the size.
 *	@head_ptr - Pointer to metahead instance of a particular metapage.
 *  @size - Size of memory chunk required in bytes
 *
 *	returns - Pointer to the suitable metanode instance. NULL on Error.
 */

static metanode_t *search_metapages( metahead_t *head_ptr, unsigned int size )
{
	metahead_t * mpage = head_ptr;
	metahead_t * new_mpage = NULL;
	metanode_t * mnode = NULL;
	int node_index = 0;

	LOG_DBG("Started search on metapages for size %d\n", size);
	while( mpage != NULL )
	{
		LOG_DBG("Searching in metapage address --> 0x%x\n", mpage);
		node_index = search_metanode( mpage, size );

		LOG_DBG("Returned node index is %d\n", node_index);
		if( node_index == ERROR_MEM )
		{
			// Out of memory exception
			break;
		}
		
		if( node_index != ERROR )
		{
			// Found a suitable metanode.
			mnode = metanode_index_to_ptr ( mpage, node_index );
			break;
		}

		// No suitable metanodes found in this metapage. 
		// Check if this was the last metapage there is. 
		if( mpage->next == NULL )
		{
			// Create new metapage at the end of the list
			LOG_DBG("Metanode search failed. No more metapages left to search. Creating new metapage\n");
			if( create_metapage( (void **)&new_mpage ) == ERROR )
			{
				break;
			} 

			mpage->next = new_mpage;
		}

		mpage = mpage->next;
	}

	LOG_DBG("Returning from search metapages with node address --> 0x%x\n", (unsigned int)mnode);
	return mnode;
}

/*
 *	search_metapages_addr - This function searches for a metapage that holds metainformation for @addr
 *	@head_ptr - Pointer to metahead instance of a particular metapage.
 *  @start_address - Word aligned start address of memory chunk to be freed. 
 *
 *  Return parameter - @page_ptr - Address of the page where metanode was found.
 *	returns - Pointer to the suitable metanode instance. NULL on Error.
 */

static metanode_t * search_metapages_addr( metahead_t *head_ptr, void * start_address, metahead_t **page_ptr, int * node_idx)
{
	unsigned long addr = (unsigned long) start_address;
	metahead_t * mpage = head_ptr;
	metanode_t * mnode = NULL;
	int node_index = 0;
	
	LOG_DBG("Started search on metapages for start address %x\n", addr);
	
	while( mpage != NULL )
	{
		LOG_DBG("Searching in metapage address --> 0x%x\n", mpage);
		node_index = search_metanode_addr( mpage, addr );

		LOG_DBG("Returned node index is %d\n", node_index);
		
		if( node_index != ERROR )
		{
			// Found a suitable metanode.
			mnode = metanode_index_to_ptr ( mpage, node_index );

			LOG_DBG("Metanode found at address %x in metapage with address %x\n", (unsigned long) mnode, (unsigned long) mpage);
			*page_ptr = mpage;
			*node_idx = node_index;

			break;
		}

		// Didn't find the correct metanode in this page. Try the next one.
		mpage = mpage->next;
	}

	if(mnode == NULL)
	{
		LOG_DBG("Looks like no suitable metapage exist at all\n");
	}

	LOG_DBG("Returning from search metapages with node address --> 0x%x\n", (unsigned int)mnode);
	return mnode;
}


/*
 *	allocate_memory - This function searches all the metapages and allocates a new memory chunk in a suitable metapage.
 *	@head_ptr - Pointer to metahead instance of a particular metapage.
 *  @size - Size of memory chunk required in bytes
 *
 *	returns - Address of memory chunk. NULL on Error.
 */

static void * allocate_memory( metahead_t *head_ptr, unsigned int size )
{
	metanode_t * mnode = NULL;
	void * addr = NULL;

	LOG_DBG("Allocating memory\n");

	// Search for a suitable metapage where memory of the requested size can be allocated
	LOG_DBG("Calling search metapages\n");
	mnode = search_metapages( head_ptr, size );
	if( mnode != NULL)
	{
		// We now have a suitable metanode. Next, allocate memory chunk. 
		LOG_DBG("Found a node to allocate data on\n");
		LOG_DBG("Allocating data block on node --> 0x%x\n", (unsigned int)mnode);
		addr = alloc_data_block( mnode, size );
	}
	else
	{
		LOG("Error: Unable to get suitable metanode for allocation.\n");
	}

	LOG_DBG("Allocated address --> 0x%x\n", (unsigned int)addr);
	LOG_DBG("Returning after allocating memory\n");
	return addr;
}

static metahead_t * get_prev_metahead (metahead_t * head, metahead_t * mpage)
{
	metahead_t * temp = head;
	metahead_t * next_page;
	unsigned int count = 0;

	while(temp != NULL)
	{
		LOG_DBG("Traversing list of metapages.. [%d]\n", ++count);
		
		next_page = temp->next;

		if((unsigned long)(next_page) == (unsigned long)(mpage))
		{
			return temp;
		}
		
		temp = next_page;
	}

	LOG_DBG("Could not find a previous page\n");

	return NULL;
}

static int meta_cleanup (metahead_t ** pHead, metahead_t ** pMpage, metanode_t * mnode, int node_index)
{
	// Re-traverse the list of metapages until we have the one that was just operated upon.
	// This is done just to get the previous node.
	metahead_t * prev_page = NULL;
	metahead_t * next_page = NULL;
	metahead_t * head = (*pHead);
	metahead_t * mpage = (*pMpage);

	LOG_DBG("Performing cleanup of metainformation\n");

	// Cleanup metanode
	if( mnode->lcfb == ((mnode->page_count * PAGE_SIZE) - (2 * sizeof(page_metadata_t))) )
	{
		// This node is not being used. Let's remove this.
		// Delete datapage before removing metanode.
		delete_data_page( &(mnode->data_page_addr), mnode->page_count );
	
		if( remove_metanode(mpage, node_index) == ERROR )
		{
			LOG("Error: Couldn't remove metanode during cleanup\n");
			return ERROR;
		}
		
	}

	if( mpage->metanode_count == 0)
	{
		// This page does not have any active metanodes
		
		LOG_DBG("mpage = %x\n", mpage);

		// Get a previous page
		prev_page = get_prev_metahead(head, mpage);
		next_page = mpage->next;
	
		// Delete the entire metapage
		if( delete_metapage((void **) (&mpage)) != SUCCESS )
		{
			LOG("Error: Metapage deletion failed\n");
			return ERROR;
		}

		*pMpage = mpage;

		LOG_DBG("Metapage deleted. Adjusting linklist pointers\n");

		//LOG_DBG("pMpage = %x\n", (*pMpage));
	    //LOG_DBG("pHead = %x\n", (*pHead));
		//LOG_DBG("Prev Page = %x\n", prev_page);
		//LOG_DBG("Next Pafe = %x\n", next_page);
#if 1
		if(prev_page != NULL)
		{
			// This means that the page that was deleted was not the head page.
			prev_page->next = next_page;
			
			LOG_DBG("The page that was deleted was not the head page. Adjusting pointers\n");
			
		}
		else // if(prev_page == NULL)
		{
			// This means that a previous page instance was not found. 
			// This could only means that the node is a head node. 

			// Modify global head node to point to the next element.
			*pHead = next_page;
			
			LOG_DBG("The page the was deleted WAS the head page. Assigning the next element in the list as the head\n");
			
		}
#else
		if(prev_page == head)
		{
			LOG_DBG("Looks like the page that was freed could have been the head\n");

			if(prev_page != NULL)
			{
				LOG_DBG("Previous metapage was the head. Sorting its next pointers out\n");
				prev_page->next = next_page;
			}
			else if( next_page == NULL)
			{
				LOG_DBG("Current metapage was the only instance. This was removed\n");
				// mpage was the only metapage. Main head pointer is set to NULL again
			}
			else
			{
				LOG_DBG("Current metapage was the head with other pages in the list\n");
				head = next_page;
				*pHead = next_page;

			}
		}

		// Assign correct next pointers before removing mpage
		else // (prev_page != head)
		{
			LOG_DBG("This metapage is somewhere in the middle\n");

			if(prev_page == NULL)
			{
				LOG("Error: Something went wrong while seeking for the latest cleaned page\n");
				return ERROR;
			}

			prev_page->next = next_page;
		}
#endif	
	}

	return SUCCESS;
}

/*
 *	free_memory - This function searches all the metapages and frees data block.
 *	@head_ptr - Pointer to metahead instance of a particular metapage.
 *  @addr - address of memory chunk to be freed
 *
 *	returns - 0 on success and -1 on Error
 */

static int free_memory( metahead_t **head_ptr, void * addr )
{
	metanode_t * mnode = NULL;
	metahead_t * mpage = NULL;
	int node_index = 0;

	LOG_DBG("Freeing memory\n");

	// Get a hold on the metahead for the address to be freed.
	LOG_DBG("Calling search metapages\n");

	mnode = search_metapages_addr( (*head_ptr), addr, &mpage, &node_index );
	if( mnode == NULL )
	{
		LOG("Error: Cannot find suitable metapage. This address might not be the result of a dmm_malloc call\n");
		return ERROR;
	}
	else
	{
		// We got a pointer to a metanode. Go an delete data block
		LOG_DBG("Found metapage and metanode. Trying to free data block\n");

		if( free_data_block( mnode, addr ) == ERROR)
		{
			LOG("Error: Something went wrong while freeing data block\n");
		}
		else
		{
			LOG_DBG("Address freed --> 0x%x\n", (unsigned int)addr);

			// Performing cleanup
			if( meta_cleanup(head_ptr, &mpage, mnode, node_index) != SUCCESS )
			{
				LOG("Error: Something went wrong while cleaning up meta information\n");
				return ERROR;
			}
		}
	}

	return SUCCESS;
}

/* Functions to use the dynamic memory manager */

/*
 *	dmm_malloc - Provides the caller with the starting virtual address of a chunk of memory from the heap, specified by @size
 *	@size - Size of memory chunk required in bytes
 *
 *	returns - Address of memory chunk from the heap
 */

void * dmm_malloc( unsigned int size )
{
	// Check for DMM initialization
	if( check_for_init( (void *)MHEAD ) == ERROR )
	{
		// If this is the first call to dmm_malloc, initialize head pointer with first metapage instance.
		if( dmm_init( (void **)&MHEAD ) == ERROR)
		{
			// Something went wrong while creating a new metapage. Return NULL.
			return NULL; 
		}
	}

	// DMM is up and active. 
	// Go ahead and allocate memory for the requested size and return the start address.
	return allocate_memory( MHEAD, size );
}


/*
 *	dmm_free - Frees a chunk of memory previously allocated using dmm_malloc
 *	@addr - Address of the memory chunk
 * 
 *  @prerequisite - The address should be the start address of the memory chunk obtained by calling dmm_malloc
 *
 *	returns - nothing
 */

void dmm_free( void * addr )
{
	if( check_for_init( (void *)MHEAD ) == ERROR )
	{
		LOG("Error: No calls to dmm_malloc have been done till now.");
		return;
	}

	if(addr == NULL)
	{
		LOG("Error: Cannot free null address\n");
	}

	if( free_memory( (void **) &MHEAD, addr ) == ERROR )
	{
		LOG("Error: Free Failed\n");
	}
#if DEBUG_BUILD
	else
	{
		LOG_DBG("Successfully freed memory!\n");
	}
#endif
	return;
}

// Parser functions for Verification

static void parser_print_metapage_info(metahead_t ** page, unsigned int index)
{
	metahead_t * temp = (metahead_t *) (*page);

	LOG("Metapage (%d) [ 0x%08x ] | C [ %d ] | Next [ 0x%08x ]\n", index, temp, temp->metanode_count, temp->next);

	return;
}

static void parser_print_metanode_info(metanode_t ** node, unsigned int index)
{
	metanode_t * temp = *node;

	LOG("\tMetanode (%d) DPAddr [ 0x%08x ] | LABAddr [ 0x%08x ] | PC [ %d ] | LCFB [ %d ]\n", 
									index,
									temp->data_page_addr, 
									temp->last_alloc_block, 
									temp->page_count, 
									temp->lcfb);

	return;
}

static char * used_or_free(unsigned int data)
{
	if(data == BLOCK_USED)
	{
		return "U";
	}
	else if(data == BLOCK_FREE)
	{
		return "F";
	}
	else
	{
		return "?";
	}

}

static void parser_print_data_page_list(metanode_t ** node)
{
	metanode_t * lNode = NULL;
	unsigned long start_addr = 0;
	page_metadata_t *pmeta = NULL;
	unsigned int data_page_size = 0;
	unsigned int total_size = 0;
	unsigned int block_count = 0;
	
	lNode = *node;
	data_page_size = (PAGE_SIZE * (lNode->page_count));

	start_addr = (unsigned long)(lNode->data_page_addr);


	while(total_size != data_page_size)
	{
		pmeta = (page_metadata_t *)(start_addr);
		start_addr += pmeta->size;
		total_size += pmeta->size;
		++block_count;
		
		LOG("\t\tBlkCnt [ %d ] | Size [ %d ] | Status [ %s ]\n", block_count, pmeta->size, used_or_free(pmeta->flag));
	}
	

	return;
}

static void parser_metapage(metahead_t ** page)
{
	metahead_t * head = *page;
	metanode_t * node = NULL;
	unsigned int i;
	unsigned int node_offset;

	for(i = 0; i < head->metanode_count; i++)
	{
		node_offset = head->index_map[i];
		node = metanode_index_to_ptr( head , node_offset ); 
	
		parser_print_metanode_info( &node , node_offset);

		parser_print_data_page_list( &node );
	}

	return;
}

void dmm_parse(void)
{
	metahead_t * head = MHEAD;
	unsigned int page_count = 0;
	
	LOG("Parsing DMM tables..\n");

	if(head == NULL)
	{
		LOG("No Allocations done yet\n");
	}
	else
	{

		while(head != NULL)
		{
			parser_print_metapage_info(&head, ++page_count);
			
			parser_metapage(&head);

			head = head->next;
		}

	}
	
	LOG("Parsing DMM done!\n");
}
