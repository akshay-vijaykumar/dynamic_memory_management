INCLUDES:= .

all: app demo

demo: mm.c mm.h demo.c
	@gcc -Wall -g3 -o demo -I $(INCLUDES) mm.c demo.c 

app: mm.c mm.h app.c
	@gcc -Wall -g3 -o app -I $(INCLUDES) mm.c app.c 

clean:
	@rm -rf app demo
